A simple Python script that parses the [site](https://somafm.com/charts/) and creates the spotify playlist.
I use this for my playlist [DEF CON Radio by SOMA FM (weekly top)](https://open.spotify.com/playlist/4YaqfNl7tLWeBKFgYw13T7) and for [others](https://open.spotify.com/playlist/0E7k7A5OD07LxB319EJpgG)

### how to use
to run, you need a [spotipy](https://spotipy.readthedocs.io/en/2.19.0/#) token (on s3 bucket) and some environment variables:
* **SPOTIPY_CLIENT_ID; SPOTIPY_CLIENT_SECRET; SPOTIPY_REDIRECT_URI**  - you can take it on [Spotify developer dashboard](https://developer.spotify.com/)
* **S3_AWS_ACCESS_KEY_ID; S3_AWS_SECRET_ACCESS_KEY; S3_ENDPOINT_URL; S3_BUCKET; S3_REGION_NAME** - credentials for s3 bucket whith Spotipy token
* **PLAYLIST_ID** - spotify playlist id (example:"4YaqfNl7tLWeBKFgYw13T7")
* **CHART_URI** - Link to chart on [SomaFm Radio](https://somafm.com/) (example: "https://somafm.com/charts/defcon/")

### Helm
First you need **values.yaml**, example of this file can be found in the **./spotify-somafm/values.yaml** (contains fake random credentials, replace with your own).
Next you create a namespace and run the chart:
```
kubectl create namespace spotify
helm install -f values.yaml spotify-somafm/ -g --namespace spotify
```
