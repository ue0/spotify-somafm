FROM python:3.9-slim
WORKDIR /app
RUN mkdir "config"
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY spotify-somafm.py .
CMD ["python3", "spotify-somafm.py"]
