import os
import re
import boto3
import spotipy
import requests
import itertools
from bs4 import BeautifulSoup
from spotipy.oauth2 import SpotifyOAuth

if os.path.isfile("config/access_token"):
    print("OK - file access_token exist")
else:
    try:
        s3 = boto3.session.Session().client("s3", region_name=os.getenv("S3_REGION_NAME"),
                                            endpoint_url=os.getenv("S3_ENDPOINT_URL"),
                                            aws_access_key_id=os.getenv("S3_AWS_ACCESS_KEY_ID"),
                                            aws_secret_access_key=os.getenv("S3_AWS_SECRET_ACCESS_KEY"))
        s3.download_file(os.getenv("S3_BUCKET"), "access_token", "config/access_token")
        print("OK - file downloaded from S3")
    except:
        print("FAIL - the file does not exist and cannot be downloaded from S3")
        exit(1)

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(cache_path='config/access_token'))
playlist_id = os.getenv('PLAYLIST_ID')
chart_uri = os.getenv('CHART_URI')

def get_list_from_url(uri):
    html = requests.get(uri).text
    soup = BeautifulSoup(html, "html.parser")
    text = str(soup.find_all("div", id="content"))
    patern = re.compile("\n\n")
    songs_list = []
    for song_name in itertools.chain(patern.split(text)[6][24:].splitlines(),
                                     patern.split(text)[7][28:].splitlines()):
        resubs = {r'&amp;': r'&', r' - ':r' ', r'^\d+\. ':r'', r'   \(\d+\)':r''}
        for p, r in resubs.items():
            song_name = re.sub(p, r, song_name)
        songs_list.append(song_name) #[3:-6]
    spotipy_list = []
    for s in songs_list:
        try:
            rock = sp.search(s, limit=1)['tracks']['items'][0]['uri']
            spotipy_list.append(rock)
            print('+', s)
        except:
            print('-', s)
    return set(spotipy_list)

sp.playlist_replace_items(playlist_id, get_list_from_url(chart_uri))

try:
    s3 = boto3.session.Session().client("s3", region_name=os.getenv("S3_REGION_NAME"),
                                            endpoint_url=os.getenv("S3_ENDPOINT_URL"),
                                            aws_access_key_id=os.getenv("S3_AWS_ACCESS_KEY_ID"),
                                            aws_secret_access_key=os.getenv("S3_AWS_SECRET_ACCESS_KEY"))
    s3.upload_file('config/access_token', os.getenv("S3_BUCKET"), 'access_token')
    print("OK - file uploaded to S3")
except:
    print("FAIL - failed on uploading to s3")
    exit(1)